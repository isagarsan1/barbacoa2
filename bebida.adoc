== Lista de bebidas

// Separadas en con/sin alcohol
// Ordenadas por orden alfabético

=== Con alcohol

* Cerveza
* Ginebra
* Tinto de verano

=== Sin alcohol

* 7up
* Agua
* Cerveza sin
* CocaCola
* Fanta de limón
* Fanta-Naranja
* Nestea
* Pepsi
